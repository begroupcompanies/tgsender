package tgsender

import (
	"errors"
	"testing"
	"time"
)

func TestGetSender(t *testing.T) {
	s, err := GetSender("", "", time.Second)
	resultError := errors.New("invalid host")

	if err == nil {
		t.Fatalf("Unexpected sender: %v, error: %v. Expected sender: %v, error: %v",
			nil, resultError,
			sender, err)
	}

	s, err = GetSender("localhost:8089", "", time.Second)
	resultError = errors.New("invalid name")

	if err == nil {
		t.Fatalf("Unexpected sender: %v, error: %v. Expected sender: %v, error: %v",
			nil, resultError,
			sender, err)
	}

	s, err = GetSender("localhost:8089", "DSP producer", time.Second)
	resultSender := Sender{
		Host:    "localhost:8089",
		Name:    "DSP producer",
		Timeout: time.Second,
	}

	if err != nil || resultSender != *s {
		t.Fatalf("Unexpected sender: %v, error: %v. Expected sender: %v, error: %v",
			resultSender, nil,
			s, err)
	}
}

func TestSender_Send(t *testing.T) {
	s, _ := GetSender("localhost:8089", "DSP producer", time.Second)
	err := s.Send("abcdef", "Example message")
	resultError := errors.New("invalid channel")
	if err == nil || err.Error() != resultError.Error() {
		t.Fatalf("Unexpected error %v. Expected error: %v", resultError, err)
	}

	err = s.Send(MonitorChannel, "")
	resultError = errors.New("invalid message")
	if err == nil || err.Error() != resultError.Error() {
		t.Fatalf("Unexpected error %v. Expected error: %v", resultError, err)
	}
}