package tgsender

import (
	"errors"
	"fmt"
	"github.com/valyala/fasthttp"
	"sort"
	"sync"
	"time"
)

const (
	// BeSeed Internal Notify канал
	// содержит служебные сообщения об ошибках отправки SMS а также об оплате кампаний
	InternalChannel = "internal"
	// BeGroup monitor канал
	// содержит сообщения о критических ошибках сервисов, а также о расхождении Impression и FirstQuartile
	MonitorChannel = "monitor"
	// BeGroup Camp Checker
	// содержит сообщения о необходимости проверки рекламных кампаний
	CampaignCheckChannel = "campcheck"
	// BeGroup Camp Notify
	// содержит сообщения о завершении рекламных кампаний
	CampaignNotifyChannel = "campnotify"
)

var availableChannel = []string{InternalChannel, MonitorChannel, CampaignCheckChannel, CampaignNotifyChannel}

var sender Sender
var once sync.Once

type Sender struct {
	Host    string
	Name    string
	Timeout time.Duration
}

// NewSender единожды создает экземпляр отправителя
// и возвращает уже созданный при всех последующих вызовах
// Возвращает ошибку, если переданы некорректные параметры
func GetSender(host string, name string, timeout time.Duration) (*Sender, error) {
	if len(host) < 5 {
		return nil, errors.New("invalid host")
	}

	if len(name) == 0 {
		return nil, errors.New("invalid name")
	}

	once.Do(func() {
		sender = Sender{
			Host:    host,
			Name:    name,
			Timeout: timeout,
		}
	})

	return &sender, nil
}

// Send Отправляет сообщение в канал Telegram
func (sender *Sender) Send(channel string, message string) error {
	if sort.SearchStrings(availableChannel, channel) == 0 {
		return errors.New("invalid channel")
	}

	if len(message) == 0 {
		return errors.New("invalid message")
	}

	var client = &fasthttp.Client{}
	req := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(req)
	req.Header.SetMethod("GET")
	message = fmt.Sprintf("[%s]: \"%s\"", sender.Name, message)

	fmt.Println(message)
	req.SetRequestURI(fmt.Sprintf("http://%s/%s?message=%s", sender.Host, channel, message))
	err := client.DoTimeout(req, nil, sender.Timeout)
	return err
}
